import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import TopBar from './pages/TopBar';
import Home from './pages/Home';
import Shop from './pages/Shop';
import HistoryBuy from './pages/HistoryBuy';
import Footer from './pages/Footer';
import ProductDetial from './pages/ProductDetial';
import Cart from './pages/Cart';
import Status from './pages/Status';


class App extends Component {
  render() {

    return (
      <Router exact path="/">
        <TopBar />
        <Route exact path='/' component={Home} />
        <Route exact path='/Shop' component={Shop} />
        <Route exact path='/HistoryBuy' component={HistoryBuy} />
        <Route exact path='/ProductDetial' component={ProductDetial} />
        <Route exact path='/Cart' component={Cart} />
        <Route exact path='/Status' component={Status} />
        <Footer />
      </Router>
    );
  }
}

export default App;
