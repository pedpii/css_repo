import React, { Component } from 'react';
import '../App.css';
import product from '../photo/slide.jpg';
import banana from '../photo/กล้วยตาก.jpg'
import { Link } from 'react-router-dom';

const mockup_product = [
  {
    pro_name: "ข้าว",//ชื่อสินค้า
    pro_image: "http://www.sandeerice.com/uploads/knowledges/1/shutterstock_81458779.jpg",//รูปสินค้า
    pro_detial: "ได้จากการสีข้าวสามชนิด คือข้าวหอมมะลิกล้อง ข้าวมะลิแดง และข้าวหอมนิลซี่งมีส่วนของจมูกข้าวที่เป็นต้นอ่อนที่มีคุณค่าทางอาหาร และอุดมด้วยวิตามินแร่ธาตุต่างๆ  เนื่องจากเป็นปลายข้าวจึงเหมาะทำเป็นข้าวบดสำหรับเด็กเล็กผู้สูงอายุ  หรือผู้ที่ต้องการย่อยง่าย ผลผลิตจากกลุ่มเกษตรกร จ.ยโสธรกลุ่มเกษตรกรสนามชัยเขต จ.ฉะเชิงเทรา  และกลุ่มเกษตรกรพร้าว จ.เชียงใหม่",//รายละเอียดสินค้า
  },
  {
    pro_name: "กล้วยตาก",//ชื่อสินค้า
    pro_image:"https://puechkaset.com/wp-content/uploads/2015/11/%E0%B8%81%E0%B8%A5%E0%B9%89%E0%B8%A7%E0%B8%A2%E0%B8%95%E0%B8%B2%E0%B8%81.jpg",//รูปสินค้า
    pro_detial: "กล้วยพันธ์ุดีจาก จ.น่าน อบน้ำผึ้งเดือน 5 เเท้ ให้กลิ่นหอมนวลนุ่มเหมือนผิวเด็ก 17-18 ",//รายละเอียดสินค้า
  },
  {
    pro_name: "ขันเงิน",//ชื่อสินค้า
    pro_image:"https://static.bigc.co.th/media/catalog/product/cache/2/image/497x497/9df78eab33525d08d6e5fb8d27136e95/8/8/8852332601262.jpg",//รูปสินค้า
    pro_detial: "ขันเงินขนาด 15 นิ้ว งานฝีมือจากชาวเชียงใหม่",//รายละเอียดสินค้า
  }

]

class Shop extends Component {

  constructor(props) {
    super(props);
    this.steta = {
      ProData: [],
      product_id: null,
      product_name: null,
      nutrient: null,
      volume: null,
      volume_type: null,
      product_status: null,
      researcher_name: null,
    };
  }


  render() {
    return (
      <div className="App">
        <div className="FontHeader">เลือกซื้อสินค้า</div>
        {/* <button className="BTNList">List</button>
        <button className="BTNGrid">Grid</button> */}
        <div className="Row">
          <div className="Column Middle">
              {
                mockup_product.map((mockup_product_element, index) => {
                  return (
                    <div className="Card">
                      <img src={mockup_product_element.pro_image} className="PhotoProduct"/>
                      <div className="FontCard">{mockup_product_element.pro_name}</div>
                      <div className="CardDetial">{mockup_product_element.pro_detial}</div>
                      <Link to={{ pathname: "/ProductDetial" }}><button className="BTNDetial">รายละเอียดเพิ่มเติม</button></Link>
                    </div>
                  )
                })
              }
          </div>
        </div>
      </div >
    );
  }
}

export default Shop;