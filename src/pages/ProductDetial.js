import React, { Component } from 'react';
import '../App.css';
import product from '../photo/slide.jpg'


const mockup_prodetial = [
    {
        detial_id: "PCODE001",//รหัสวัตถุดิบ
        detial_name: "กล้วย",//ชื่อวัตถุดิบ
        detial_volume: 20,//ปริมาณ
        detial_volume_type: "Kg.",//หน่วย
    },
    {
        detial_id: "PCODE123",//รหัสวัตถุดิบ
        detial_name: "น้ำผึ้ง",//ชื่อวัตถุดิบ
        detial_volume: 10,//ปริมาณ
        detial_volume_type: "Kg.",//หน่วย
    }
]
class ProductDetial extends Component {

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    AddProduct = () => {
        alert("เพิ่มสินค้าเรียบร้อย")
        console.log(this.state)
    }

    render() {
        return (
            <div className="App">
                <div className="FontHeader">รายละเอียด</div>
                <div className="Row">
                    <div className="Column Half">
                        <img src={product} className="PhotoProduct" />
                        <table>
                            <tr>
                                <th>ลำดับ</th>
                                <th>รหัสวิตถุดิบ</th>
                                <th>ชื่อวัตถุดิบ</th>
                                <th>จำนวนที่ต้องใช้ผลิต</th>
                                <th>หน่วยนับ</th>
                            </tr>
                            {
                                mockup_prodetial.map((mockup_prodetial_element, index) => {
                                    return (
                                    <tr>
                                        <td>{index+1}</td>
                                        <td>{mockup_prodetial_element.detial_id}</td>
                                        <td>{mockup_prodetial_element.detial_name}</td>
                                        <td>{mockup_prodetial_element.detial_volume}</td>
                                        <td>{mockup_prodetial_element.detial_volume_type}</td>
                                    </tr>)
                                })
                            }
                        </table>
                    </div>
                    <div className="Column Half">
                        <div className="FontBody">ปลายข้าวสามกล้องเกษตรอินทรีย์</div>
                        <div className="FontDetial">ได้จากการสีข้าวสามชนิด คือข้าวหอมมะลิกล้อง ข้าวมะลิแดง และข้าวหอมนิล
                                    ซี่งมีส่วนของจมูกข้าวที่เป็นต้นอ่อนที่มีคุณค่าทางอาหาร และอุดมด้วยวิตามิน
                                    แร่ธาตุต่างๆ  เนื่องจากเป็นปลายข้าวจึงเหมาะทำเป็นข้าวบดสำหรับเด็กเล็ก
                                    ผู้สูงอายุ  หรือผู้ที่ต้องการย่อยง่าย ผลผลิตจากกลุ่มเกษตรกร จ.ยโสธร
                                    กลุ่มเกษตรกรสนามชัยเขต จ.ฉะเชิงเทรา  และกลุ่มเกษตรกรพร้าว จ.เชียงใหม่
                        </div>
                        <input type="number" id="volume" onChange={this.handleChange} />
                        <button className="BTNAddProduct" onClick={() => { this.AddProduct() }}>เพิ่มในตะกร้า</button>


                    </div>
                </div>
            </div>
        );
    }
}

export default ProductDetial;
