import React, { Component } from 'react';
import '../App.css';
import { withRouter, NavLink } from 'react-router-dom';

class TopBar extends Component {
    render() {
        return (
            <div className="App Sticky">
                <div className="TopBar">
                    <div className="Logo">LOGO</div>
                    <NavLink exact to="/" activeClassName="active" className="TopBarLeft">หน้าเเรก</NavLink>
                    <NavLink to="/Shop" activeClassName="active" className="TopBarLeft">เลือกซื้อสินค้า</NavLink>
                    <NavLink to="/HistoryBuy" activeClassName="active" className="TopBarLeft">ประวัติการซื้อสินค้า</NavLink>
                    <NavLink to="/Cart" activeClassName="active" className="TopBarLeft">ตระกร้าสินค้า<span className="Badge">0</span></NavLink>
                    <NavLink to="#" className="TopBarRight">ผู้ประกอบการ</NavLink>
                </div>
            </div>
        );
    }
}
export default withRouter(TopBar);
