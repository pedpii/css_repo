import React, { Component } from 'react';

class Checkbox extends Component {
    state = {
        isChecked: false,
    }

    toggleCheckboxChange = () => {
        const { handleCheckboxChange, label } = this.props;

        this.setState(({ isChecked }) => (
            {
                isChecked: !isChecked,
            }
        ));

        handleCheckboxChange(label);
    }

    onCheck = (event) => {
        console.log("Check", event.target.value)


        let check_array = this.props.check_array
        let index = check_array.findIndex((array_event)=>{
            return array_event === event.target.value
        })
        console.log("index",index)

        if(index !== -1){
            check_array.splice(index,1)
        }
        else{
            check_array.push(event.target.value)
        }

        this.props.return_func(check_array)
    }



    render() {

        return (
            <div className="checkbox">
                {
                    this.props.option.map((option_element, index) => {
                        return (
                            <div>
                                <input type="checkbox" value={option_element.value}
                                    onClick={(event) => { this.onCheck(event) }} />
                                {option_element.name}
                            </div>
                        )
                    })
                }

            </div>
        );
    }
}

// Checkbox.propTypes = {
//   label: PropTypes.string.isRequired,
//   handleCheckboxChange: PropTypes.func.isRequired,
// };

export default Checkbox;