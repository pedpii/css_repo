import React, { Component } from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import photo_profile from '../photo/default-user-image.png';
import photo_file from '../photo/file.png';


const mockup_list = [
  {
    order_id: "SE001",//SE001
    order_date: "20/1/60",//full date
    order_status: 0,// 0 = กำลังดำเนินการ , 1 = สำเร็จเเล้ว
  },
  {
    order_id: "SE002",
    order_date: "20/1/60",
    order_status: 1,
  },
  {
    order_id: "SE003",
    order_date: "20/1/60",
    order_status: 1,
  }
]

class HistoryBuy extends Component {

  //แปลงค่าที่รับมาเป็นตัวเลข เเสดงเป็นตัวหนังสือ
  render_status = (order_status) => {
    let render_tag

    switch (order_status) {
      case 0:
        render_tag = <div className="FontWarning" > กำลังดำเนินการ </div>
        break;
      case 1:
        render_tag = <div className="FontSuccess"> สำเร็จแล้ว </div>
        break;
      default:
        render_tag = <div > เกิดข้อผิดพลาด </div>
        break;
    }
    return render_tag
  }

  render() {

    return (
      <div className="App">
        <div className="FontHeader">ประวัติการซื้อ</div>
        <div className="Row">
          
          <div className="Column Side">
            <img className="PhotoProfile" src={photo_profile} />
            <div className="FontBody">Name</div>
            <ul>
              <li><button className="BTNProcess">กำลังดำเนินการ</button></li>
              <li><button className="BTNSuccess">สำเร็จเเล้ว</button></li>
            </ul>

          </div>
          <div className="Column Middle">
            <table>
              <tbody>
                <td> <input type="search" /></td>
                <td><input type="date" /></td>
                <td>
                  <select className="Select">
                    <option>ล่าสุด</option>
                    <option>เก่าสุด</option>
                  </select>
                </td>
                <td><button className="BTNSearch">ค้นหา</button></td>
              </tbody>
              <tr>
                <th>รหัสใบสั่งซื้อ</th>
                <th>วันที่</th>
                <th>สถานะ</th>
                <th>ไฟล์</th>
              </tr>

              {
                mockup_list.map((mockup_list_element, mockup_list_index) => {
                  return (
                    <tr>
                      <td>{mockup_list_element.order_id}</td>
                      <td>{mockup_list_element.order_date}</td>
                      <td>{this.render_status(mockup_list_element.order_status)}</td>
                      <td><Link to={{ pathname: "/Status" }}><img src={photo_file} /></Link></td>
                    </tr>
                  )
                })
              }

            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default HistoryBuy;