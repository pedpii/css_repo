import React, { Component } from 'react';
import '../App.css';

const mockup_cart = [
    {
        pro_id: "PCODE0001",//รหัสสินค้า
        pro_name: "กล้วยไข่",//ชื่อสินค้า
        pro_volume: 120,//จำนวน
    },
    {
        pro_id: "PCODE0002",//รหัสสินค้า
        pro_name: "กล้วยตาก",//ชื่อสินค้า
        pro_volume: 120,//จำนวน
    },

]

const mockup_nutrie = [
    {
        nut_id: "NCODE123",//รหัสวัตถุดิบ
        nut_name: "กล้วย",//ชื่อวัตถุดิบ
        nut_volume: 1500,//ปริมาาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    },
    {
        nut_id: "NCODE13",//รหัสวัตถุดิบ
        nut_name: "น้ำผึ้ง",//ชื่อวัตถุดิบ
        nut_volume: 150,//ปริมาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    },
    {
        nut_id: "NCODE13",//รหัสวัตถุดิบ
        nut_name: "น้ำผึ้ง",//ชื่อวัตถุดิบ
        nut_volume: 150,//ปริมาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    }
]

class Status extends Component {
    render() {

        return (
            <div className="App">
                <div className="FontHeader">สถานะสินค้า</div>
                <div className="Row">
                    <div className="Colume Side" />
                    <div className="Column Middle">
                        <div className="Status">สถานะทั้ง 5 ใบ
                            ใบสั่งซื้อ/
                            ใบเสนอราคา/
                            ใบเเจ้งหนี้/
                            ใบเสร็จ/
                            ยืนยันการรับสินค้า
                        </div>
                        <div>
                            แสดง PDF ไฟล์ใบล่าสุด
                            <button className="BTNComfirm">ยืนยัน</button>
                        </div>
                        <div className="Status">
                            <div className="Timeline">
                                <div className="Container FontStatus">
                                    20/01/2019 01:20
                                    <div>ยืนยันการรับสินค้า</div>
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20
                                    <div>SE กลางส่งใบเสร็จและสินค้า</div>
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>SE กลางเตรียมการจัดส่งสินค้า</div>
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>ทำการยืนยันใบเเจ้งหนี้(โอนเงิน)</div>
                                </div>    
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>SE กลางส่งใบเเจ้งหนี้</div>
                                </div>    
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>ทำการยืนยันการใบเสนอราคา</div>
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>SE กลางส่งใบเสนอราคา(ยังไม่มีที่อยู่ มีเเต่ราคาของวัตถุดิบ)</div>                                    
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>SE กลางได้รับการสั่งซื้อ</div>
                                </div>
                                <div className="Container FontStatus">
                                    20/01/2019 01:20 
                                    <div>มีคำสั่งซื้อใหม่</div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <table>
                                <tr>

                                    <th rowspan="2">ลำดับ</th>
                                    <th rowspan="2">รหัสสินค้า</th>
                                    <th rowspan="2">ชื่อสินค้า</th>
                                    <th rowspan="2">จำนวน</th>
                                    <th colSpan="10">รายละเอียดวัตถุดิบ</th>
                                </tr>
                                <tr>

                                    <th>ลำดับ</th>
                                    <th>รหัสวัตถุดิบ</th>
                                    <th>ชื่อวัตถุดิบ</th>
                                    <th>ปริมาณวัตถุดิบ</th>
                                    <th>หน่วยนับ</th>
                                </tr>

                                {
                                    mockup_cart.map((mockup_Cart_element, index) => {
                                        return (
                                            <tr>

                                                <td>{index + 1}</td>
                                                <td>{mockup_Cart_element.pro_id}</td>
                                                <td>{mockup_Cart_element.pro_name}</td>
                                                <td>{mockup_Cart_element.pro_volume}</td>
                                                <td>{mockup_nutrie.map((nut_element, nut_index) => {
                                                    return (
                                                        <div>{nut_index + 1}</div>
                                                    )
                                                })
                                                }
                                                </td>
                                                <td>{mockup_nutrie.map((nut_element, index) => {
                                                    return (
                                                        <div>{nut_element.nut_id}</div>
                                                    )
                                                })
                                                }
                                                </td>
                                                <td>{mockup_nutrie.map((nut_element, index) => {
                                                    return (
                                                        <div>{nut_element.nut_name}</div>
                                                    )
                                                })
                                                }
                                                </td>
                                                <td>{mockup_nutrie.map((nut_element, index) => {
                                                    return (
                                                        <div>{nut_element.nut_volume}</div>
                                                    )
                                                })
                                                }
                                                </td>
                                                <td>{mockup_nutrie.map((nut_element, index) => {
                                                    return (
                                                        <div>{nut_element.nut_volume_type}</div>
                                                    )
                                                })
                                                }
                                                </td>

                                            </tr>
                                        )
                                    })
                                }


                            </table>
                        </div>
                    </div>
                    <div className="Colume Side" />

                </div>
            </div>
        );
    }
}

export default Status;