import React, { Component } from 'react';
import '../App.css';

const mockup_cart = [
    {
        pro_id: "PCODE0001",//รหัสสินค้า
        pro_name: "กล้วยไข่",//ชื่อสินค้า
        pro_volume: 120,//จำนวน
    },
    {
        pro_id: "PCODE0002",//รหัสสินค้า
        pro_name: "กล้วยตาก",//ชื่อสินค้า
        pro_volume: 120,//จำนวน
    },

]

const mockup_nutrie = [
    {
        nut_id: "NCODE123",//รหัสวัตถุดิบ
        nut_name: "กล้วย",//ชื่อวัตถุดิบ
        nut_volume: 1500,//ปริมาาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    },
    {
        nut_id: "NCODE13",//รหัสวัตถุดิบ
        nut_name: "น้ำผึ้ง",//ชื่อวัตถุดิบ
        nut_volume: 150,//ปริมาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    },
    {
        nut_id: "NCODE13",//รหัสวัตถุดิบ
        nut_name: "น้ำผึ้ง",//ชื่อวัตถุดิบ
        nut_volume: 150,//ปริมาณวัตถุดิบ
        nut_volume_type: "Kg.",//หน่วยนับ
    }
]


class Cart extends Component {

    Comfirm = () => {
        alert("ระบบดำเนินการส่งใบสั่งซื้อเรียบร้อย")
    }

    PDF = () => {
        alert("Flie PDF")
    }

    Delete = () => {
        alert("ลบสินค้าเเล้ว")
    }
    render() {
        return (
            <div className="App">
                <div className="FontHeader">ตระกร้าสินค้า</div>
                <div className="Row">
                    <div className="Column">
                        ชื่อบริษัท รหัสใบสั่งซื้อสินค้า วันที่ รวมจำนวนชิ้นสินค้า

                        <table>
                            <tr>
                                <th rowspan="2"><input type="checkbox"/></th>
                                <th rowspan="2">ลำดับ</th>
                                <th rowspan="2">รหัสสินค้า</th>
                                <th rowspan="2">ชื่อสินค้า</th>
                                <th rowspan="2">จำนวน</th>
                                <th colSpan="10">รายละเอียดวัตถุดิบ</th>
                            </tr>
                            <tr>
                                <th>ลำดับ</th>
                                <th>รหัสวัตถุดิบ</th>
                                <th>ชื่อวัตถุดิบ</th>
                                <th>ปริมาณวัตถุดิบ</th>
                                <th>หน่วยนับ</th>
                                <td></td>
                                <td></td>
                            </tr>

                            {
                                mockup_cart.map((mockup_Cart_element, index) => {
                                    return (
                                        <tr>
                                            <td><input type="checkbox"/></td>
                                            <td>{index + 1}</td>
                                            <td>{mockup_Cart_element.pro_id}</td>
                                            <td>{mockup_Cart_element.pro_name}</td>
                                            <td>{mockup_Cart_element.pro_volume}</td>
                                            <td>{mockup_nutrie.map((nut_element, nut_index) => {
                                                return (
                                                    <div>{nut_index + 1}</div>
                                                )
                                            })
                                            }
                                            </td>
                                            <td>{mockup_nutrie.map((nut_element, index) => {
                                                return (
                                                    <div>{nut_element.nut_id}</div>
                                                )
                                            })
                                            }
                                            </td>
                                            <td>{mockup_nutrie.map((nut_element, index) => {
                                                return (
                                                    <div>{nut_element.nut_name}</div>
                                                )
                                            })
                                            }
                                            </td>
                                            <td>{mockup_nutrie.map((nut_element, index) => {
                                                return (
                                                    <div>{nut_element.nut_volume}</div>
                                                )
                                            })
                                            }
                                            </td>
                                            <td>{mockup_nutrie.map((nut_element, index) => {
                                                return (
                                                    <div>{nut_element.nut_volume_type}</div>
                                                )
                                            })
                                            }
                                            </td>
                                            <td><button className="BTNCencleProduct" onClick={() => { this.Delete() }}>ลบ</button></td>
                                            <td><button className="BTNEdit">แก้ไข</button></td>
                                        </tr>
                                    )
                                })
                            }


                        </table>
                        
                        <button className="BTNComfirm" onClick={() => { this.Comfirm() }}>ยืนยันใบสินค้า</button>
                        <button className="BTNPreview" onClick={() => { this.PDF() }}>ดูตัวอย่างใบสั่งซื้อ</button>
                        <button className="BTNDelete" onClick={() => { this.Delete() }}>ลบที่เลือก</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Cart;
